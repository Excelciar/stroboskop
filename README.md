# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
((UKAZI GIT))
git clone https://excelciar@bitbucket.org/excelciar/stroboskop.git
git init
```

Naloga 6.2.3:
https://bitbucket.org/Excelciar/stroboskop/commits/e9e5a15d3b09c9256481789cafea43f9cf0ebd71

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Excelciar/stroboskop/commits/8b62f3f3e501fc2e9056681c541a2a0285550618

Naloga 6.3.2:
https://bitbucket.org/Excelciar/stroboskop/commits/3b0478c783bc5f6a95c739e7a706510a2ac42b7d

Naloga 6.3.3:
https://bitbucket.org/Excelciar/stroboskop/commits/94dba741b66ac7075cf0e8be627703ab8e3c1177

Naloga 6.3.4:
https://bitbucket.org/Excelciar/stroboskop/commits/2871a9c2b37100be391b49bea0bd8d1af748ceb7

Naloga 6.3.5:

```
((UKAZI GIT))
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Excelciar/stroboskop/commits/12848d4eb095658ab8af2a49276d9768fbb0555c

Naloga 6.4.2:
https://bitbucket.org/Excelciar/stroboskop/commits/2bda58a6c9218dc6523a39571372858ef7e362e0

Naloga 6.4.3:
https://bitbucket.org/Excelciar/stroboskop/commits/fed5666591cc63d021645da2b2f4f808544d48b7

Naloga 6.4.4:
https://bitbucket.org/Excelciar/stroboskop/commits/cbd8de6cf6e3ef519dc06ed551a7c043e9f40160